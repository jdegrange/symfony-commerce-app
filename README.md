# Symfony Docker-Development-Stack
### How to install ?
#### Build images & run containers
First, create a `.env` file in the root folder with your UID & GID
```
touch .env 
echo "DEV_UID=$(id -u)" >> .env
echo "DEV_GID=$(id -g)" >> .env
```
Then, build images & run containers 
```
 docker-compose up -d
```

#### Install app
```
// connect to cli container
docker exec -ti php sh

// and install dependencies with
composer install

// install yarn dependencies & build assets
yarn install
yarn dev
```


#### Setting up database
First, create database, then create tables, and finally load fixtures datas
```
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate
```

#### Load fixtures data
```
bin/console doctrine:fixtures:load
```

Now you should be able to access :
- front url : http://127.0.0.1/
- admin url : http://127.0.0.1/admin

### How to run tests ?
First, create database, then create tables, and finally load fixtures datas for test env
```
bin/console doctrine:database:create --if-not-exists --env=test
bin/console doctrine:migrations:migrate --env=test
bin/console doctrine:fixtures:load --env=test
```
Then, run tests 
``` 
./bin/phpunit
```
### How to compil assets
Assets can be modified in the assets/styles folder for css, and app.js for javascript, then to compile them, you need to execute 
``` 
// to automatically re-compile assets when files change
yarn watch
 
// to compile assets once
yarn dev
```
They will be exported in public/build folder

### Project Features
Simple e-commerce project with :
- product entity with id, name, description, image, slug and price
- homepage display all products
- detail page for product
- basket management 
  - choose quantity on product page
  - edit quantity in basket page
  - clear basket in basket page
  - remove product
- admin backoffice (easyAdmin)
  - role_admin with one user in_memory
  - product list with full CRUD
- api route to export all products in json format with serializer : http://127.0.0.1/api/products
