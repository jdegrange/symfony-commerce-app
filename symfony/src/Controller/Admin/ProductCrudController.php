<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized()

            // set this option if you prefer the sidebar (which contains the main menu)
            // to be displayed as a narrow column instead of the default expanded design
            ->renderSidebarMinimized()
            // the labels used to refer to this entity in titles, buttons, etc.
            ->setEntityLabelInSingular('Product')
            ->setEntityLabelInPlural('Products')

            // in addition to a string, the argument of the singular and plural label methods
            // can be a closure that defines two nullable arguments: entityInstance (which will
            // be null in 'index' and 'new' pages) and the current page name
//            ->setEntityLabelInSingular(
//                fn (?Product $product, ?string $pageName) => $product ? $product->toString() : 'Product'
//            )
//            ->setEntityLabelInPlural(function (?Category $category, ?string $pageName) {
//                return 'edit' === $pageName ? $category->getLabel() : 'Categories';
//            })

            // the Symfony Security permission needed to manage the entity
            // (none by default, so you can manage all instances of the entity)
            ->setEntityPermission('ROLE_ADMIN')
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            // Hide Id when creating product cause it's an auto-generated field by doctrine
            IdField::new('id')->hideWhenCreating(),
            TextField::new('name'),
            TextareaField::new('description')->hideOnIndex(),
            TextField::new('slug'),
            NumberField::new('price')->setNumberFormat('%.2d €'),
            ImageField::new('image', 'Visuel')
                ->setUploadDir('public/images/')
            ->setBasePath('images/'),
        ];
    }
}
