<?php

namespace App\Controller;

use App\Form\BasketType;
use App\Manager\BasketManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    /**
     * Basket page, allow to remove product, update quantities and clear basket
     *
     * @param BasketManager $basketManager
     * @param Request $request
     * @return Response
     */
    #[Route('/basket', name: 'basket')]
    public function index(BasketManager $basketManager, Request $request): Response
    {
        $basket = $basketManager->getCurrentBasket();
        $form = $this->createForm(BasketType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $basketManager->save($basket);

            return $this->redirectToRoute('basket');
        }

        return $this->render('basket/detail.html.twig', [
            'basket' => $basket,
            'form' => $form->createView()
        ]);
    }
}
