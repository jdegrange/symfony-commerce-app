<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Homepage, display all products
     *
     * @param ProductRepository $productRepository
     * @return Response
     */
    #[Route('/', name: 'home')]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'products' => $productRepository->findBy(
                // emtpy cause we want all products
                [],
                // order by name
                ['name' => 'ASC']
            ),
        ]);
    }
}
