<?php

namespace App\Controller;

use App\Entity\OrderItem;
use App\Entity\Product;
use App\Form\AddToBasketType;
use App\Manager\BasketManager;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProductController extends AbstractController
{
    /**
     * Display all products
     *
     * @param ProductRepository $productRepository
     * @return Response
     */
    #[Route('/products', name: 'products')]
    public function allProducts(ProductRepository $productRepository): Response
    {
        return $this->render('product/all.html.twig', [
            'products' => $productRepository->findBy(
            // emtpy cause we want all products
                [],
                // order by name
                ['name' => 'ASC']
            ),
        ]);
    }

    /**
     * Display product page fetched by ID, after redirect to slug URI
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    #[Route('/product/{id}', name: 'product_detail', requirements: ['id' => '\d+'])]
    public function showById(Product $product, Request $request): Response
    {
         return $this->redirectToRoute('product_detail_slug', ['slug' => $product->getSlug()]);
    }

    /**
     * Display product page fetched by slug
     *
     * @param Product $product
     * @param Request $request
     * @param BasketManager $basketManager
     * @return Response
     */
    #[Route('/product/{slug}', name: 'product_detail_slug')]
    public function showBySlug(Product $product, Request $request, BasketManager $basketManager): Response
    {
        $form = $this->createForm(AddToBasketType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var OrderItem $orderItem
             */
            $orderItem = $form->getData();
            // assign current product to orderItem
            $orderItem->setProduct($product);

            // get current basket
            $basket = $basketManager->getCurrentBasket();
            $basket->addItem($orderItem);

            $basketManager->save($basket);

            // set flash messages
            $request->getSession()->getFlashBag()->add('notice', 'product_added_success');

            return $this->redirectToRoute('product_detail_slug', ['slug' => $product->getSlug()]);
        }

        return $this->render('product/detail.html.twig', [
            'product' => $product,
            'form' => $form->createView()
        ]);
    }

    /**
     * Api route to expose all product in Json format
     *
     * @param ProductRepository $productRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/api/products', name: 'products_api')]
    public function exportJson(ProductRepository $productRepository, SerializerInterface $serializer): JsonResponse {

        $products = $productRepository->findAll();

        $jsonPRoducts = $serializer->serialize($products, 'json');

        return new JsonResponse($jsonPRoducts, Response::HTTP_OK, [], true);
    }
}
