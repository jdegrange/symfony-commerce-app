<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class TalkspiritController
{
    #[Route('/talkspirit', name: 'talkspirit')]
    public function index(): JsonResponse
    {
        $ret = [
            'title' => 'Mon titre',
            'body' => 'Le contenu'
        ];

        return new JsonResponse($ret);
    }

    public function fizzBuzz(int $value): string {

        if($value % 5 == 0 && $value % 3 == 0){
            return 'fizzBuzz';
        } elseif($value % 3 == 0) {
            return 'fizz';
        } elseif ($value % 5 == 0) {
            return 'buzz';
        }

        return ''.$value;
    }

}
