<?php
namespace App\EntityListener;

use App\Entity\Product;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductEntityListener
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * Init slug value before persist
     **/
    public function prePersist(Product $product, LifecycleEventArgs $event)
    {
        $product->computeSlug($this->slugger);
    }

    /**
     * Update slug value in case name change
     **/
    public function preUpdate(Product $product, LifecycleEventArgs $event)
    {
        $product->computeSlug($this->slugger);
    }
}