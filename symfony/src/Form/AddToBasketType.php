<?php

namespace App\Form;

use App\Entity\OrderItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddToBasketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // set attr min to 1 so we can's choose negative value
        $builder->add('quantity',
            IntegerType::class, [
                'attr' => [
                    'min' => 1,
                ]
            ]);
        $builder->add('add', SubmitType::class, [
            'label' => 'Ajouter au panier'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // link form type to OrderItem entity
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
        ]);
    }
}
