<?php

namespace App\Form;

use App\Entity\Order;
use App\Form\EventListener\ClearBasketListener;
use App\Form\EventListener\RemoveBasketItemListener;
use App\Manager\BasketManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BasketType extends AbstractType
{
    public static string $BASKET_SUBMIT_TYPE_CLEAR = "clear";
    public static string $BASKET_SUBMIT_TYPE_SAVE = "save";

    private BasketManager $basketManager;

    public function __construct(BasketManager $basketManager)
    {
        $this->basketManager = $basketManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('items', CollectionType::class, [
                'entry_type' => BasketLineType::class
            ])
            ->add(self::$BASKET_SUBMIT_TYPE_SAVE, SubmitType::class)
            ->add(self::$BASKET_SUBMIT_TYPE_CLEAR, SubmitType::class)
            ->addEventSubscriber(new RemoveBasketItemListener($this->basketManager))
            ->addEventSubscriber(new ClearBasketListener($this->basketManager));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // this will link Basket data to an Order entity
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
