<?php

namespace App\Form\EventListener;

use App\Entity\Order;
use App\Form\BasketType;
use App\Manager\BasketManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class ClearBasketListener
 * @package App\Form\EventListener
 */
class ClearBasketListener implements EventSubscriberInterface
{
    private BasketManager $basketManager;

    public function __construct(BasketManager $basketManager)
    {
        $this->basketManager = $basketManager;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [FormEvents::POST_SUBMIT => 'postSubmit'];
    }

    /**
     * Manage clear button from basket
     *
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event): void
    {
        $form = $event->getForm();
        $basket = $form->getData();

        if (!$basket instanceof Order) {
            return;
        }

        // If the clear button is clicked
        if (!$form->get(BasketType::$BASKET_SUBMIT_TYPE_CLEAR)->isClicked()) {
            return;
        }

        // Clears the basket by removing all items
        $this->basketManager->clear($basket);
    }
}