<?php

namespace App\Form\EventListener;

use App\Entity\Order;
use App\Manager\BasketManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class RemoveBasketItemListener
 * @package App\Form\EventListener
 */
class RemoveBasketItemListener implements EventSubscriberInterface
{
    private BasketManager $basketManager;

    public function __construct(BasketManager $basketManager)
    {
        $this->basketManager = $basketManager;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [FormEvents::POST_SUBMIT => 'postSubmit'];
    }

    /**
     * Manage 'remove' buttons on each order item from basket
     *
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event): void
    {
        $form = $event->getForm();
        $basket = $form->getData();

        if (!$basket instanceof Order) {
            return;
        }

        // Manage remove button click from basket
        foreach ($form->get('items')->all() as $child) {
            if ($child->get('remove')->isClicked()) {
                $basket->removeItem($child->getData());
                $this->basketManager->save($basket);
                break;
            }
        }
    }
}