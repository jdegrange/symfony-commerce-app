<?php

namespace App\Manager;

use App\Entity\Order;
use App\Factory\OrderFactory;
use App\Service\BasketSessionService;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BasketManager
 * @package App\Manager
 */
class BasketManager
{
    /**
     * @var BasketSessionService
     */
    private $basketSessionService;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * BasketManager constructor.
     *
     * @param BasketSessionService $basketSessionService
     * @param OrderFactory $orderFactory
     */
    public function __construct(
        BasketSessionService $basketSessionService,
        OrderFactory $orderFactory,
        EntityManagerInterface $entityManager
    ) {
        $this->basketSessionService = $basketSessionService;
        $this->orderFactory = $orderFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * Gets the current basket.
     *
     * @return Order
     */
    public function getCurrentBasket(): Order
    {
        // get basket from session
        $basket = $this->basketSessionService->getBasket();

        if (!$basket) {
            // if none exist in session, create one basket, which is an order with status 'basket'
            $basket = $this->orderFactory->create();
        }

        return $basket;
    }

    /**
     * Save the basket in database and session.
     *
     * @param Order $basket
     */
    public function save(Order $basket): void
    {
        // update updateAt with current time every time basket is saved
        $basket->setUpdatedAt(new \DateTime());
        // save in database
        $this->entityManager->persist($basket);
        $this->entityManager->flush();

        // save in session
        $this->basketSessionService->setBasket($basket);
    }

    /**
     * Clear (empty) the basket
     *
     * @param Order $basket
     */
    public function clear(Order $basket): void
    {
        $basket->setUpdatedAt(new \DateTime());
        $basket->removeItems();

        // save in database
        $this->entityManager->persist($basket);
        $this->entityManager->flush();

        // save in session
        $this->basketSessionService->setBasket($basket);
    }
}