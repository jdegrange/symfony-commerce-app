<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use App\Repository\OrderRepository;
use App\Entity\Order;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BasketSessionService
{

    /**
     * @var string
     */
    const BASKET_KEY = 'basket_id';

    /**
     * Request stack
     *
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * The order repository.
     *
     * @var OrderRepository
     */
    private OrderRepository $orderRepository;

    /**
     * BasketSessionStorage constructor.
     *
     * @param RequestStack $requestStack
     * @param OrderRepository $orderRepository
     */
    public function __construct(RequestStack $requestStack, OrderRepository $orderRepository)
    {
        $this->requestStack = $requestStack;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Gets the basket in session.
     *
     * @return Order|null
     */
    public function getBasket(): ?Order
    {
        return $this->orderRepository->findOneBy([
            'id' => $this->getBasketId(),
            'status' => Order::STATUS_BASKET
        ]);
    }

    /**
     * Sets the basket in session.
     *
     * @param Order $order
     */
    public function setBasket(Order $order): void
    {
        $this->getSession()->set(self::BASKET_KEY, $order->getId());
    }

    /**
     * Returns basket id
     *
     * @return int|null
     */
    private function getBasketId(): ?int
    {
        return $this->getSession()->get(self::BASKET_KEY);
    }

    private function getSession(): SessionInterface
    {
        return $this->requestStack->getSession();
    }
}