<?php

namespace App\Tests;

use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\DomCrawler\Crawler;

class BasketControllerTest extends WebTestCase
{
    // CSS class of one order line added in basket
    private static $BASKET_ORDER_ITEM_CLASS = '.col-md-8 li.list-group-item';
    private static $BASKET_ORDER_TOTAL_CLASS = '.col-md-4 .list-group-item span';

    /**
     * Test to valid that basket is empty on basket page
     */
    public function testBasketIsEmpty()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/basket');

        $this->assertResponseIsSuccessful();
        $this->assertBasketIsEmpty($crawler);
    }

    /**
     * Test that will :
     *  - Select a random product from products page
     *  - Valid that basket contain exactly 1 item
     *  - Valid that basket contain previously added product
     *  - Valid that basket total is exactly product price
     */
    public function testAddProductToBasket()
    {
        $client = static::createClient();
        $product = $this->addRandomProductToBasket($client);
        $crawler = $client->request('GET', '/basket');

        $this->assertResponseIsSuccessful();
        $this->assertBasketItemsCountEquals($crawler, 1);
        $this->assertBasketContainsProductWithQuantity($crawler, $product['name'], 1);
        $this->assertBasketTotalEquals($crawler, $product['price']);
    }

    /**
     * Test that basket is empty
     */
    private function assertBasketIsEmpty(Crawler $crawler)
    {
        $infoText = $crawler
            ->filter('.alert-info')
            ->first()
            ->text(null, true);

        Assert::assertEquals(
            'Le panier est vide.',
            $infoText,
            "The basket should be empty."
        );
    }

    /**
     * Test that basket total is as expected
     */
    private function assertBasketTotalEquals(Crawler $crawler, $expectedTotal)
    {
        $actualTotal = (float)$crawler
            ->filter(self::$BASKET_ORDER_TOTAL_CLASS)
            ->first()
            ->text(null, true);

        Assert::assertEquals(
            $expectedTotal,
            $actualTotal,
            "The basket total should be equal to \"$expectedTotal €\". Actual: \"$actualTotal €\"."
        );
    }

    /**
     * Test items count in basket
     */
    private function assertBasketItemsCountEquals(Crawler $crawler, $expectedCount): void
    {
        $actualCount = $crawler
            ->filter(self::$BASKET_ORDER_ITEM_CLASS)
            ->count();

        Assert::assertEquals(
            $expectedCount,
            $actualCount,
            "The basket should contain \"$expectedCount\" item(s). Actual: \"$actualCount\" item(s)."
        );
    }

    /**
     * Select a random product from products page and add it to basket
     */
    private function addRandomProductToBasket(AbstractBrowser $client, int $quantity = 1): array
    {
        $product = $this->getRandomProduct($client);

        $crawler = $client->request('GET', $product['url']);
        $form = $crawler->filter('form')->form();
        $form->setValues(['add_to_basket[quantity]' => $quantity]);

        $client->submit($form);

        return $product;
    }

    /**
     * Return random product from homepage products list
     */
    private function getRandomProduct(AbstractBrowser $client): array
    {
        $crawler = $client->request('GET', '/');
        // get random card element in homepage, which is a product
        $productNode = $crawler->filter('.card')->eq(rand(0, 11));
        $productName = $productNode->filter('.card-title')->first()->text(null, true);
        $productPrice = (float)$productNode->filter('span.h5')->first()->text(null, true);
        $productLink = $productNode->filter('.btn-dark')->link();

        return [
            'name' => $productName,
            'price' => $productPrice,
            'url' => $productLink->getUri()
        ];
    }

    private function assertBasketContainsProductWithQuantity(Crawler $crawler, string $productName, int $expectedQuantity): void
    {
        $actualQuantity = (int)self::getItemByProductName($crawler, $productName)
            ->filter('input[type="number"]')
            ->attr('value');

        Assert::assertEquals($expectedQuantity, $actualQuantity);
    }

    private function getItemByProductName(Crawler $crawler, string $productName)
    {
        // loop through order item list to select product by name
        $items = $crawler->filter(self::$BASKET_ORDER_ITEM_CLASS)->reduce(
            function (Crawler $node) use ($productName) {
                if ($node->filter('h5')->getNode(0)->textContent === $productName) {
                    return $node;
                }

                return false;
            }
        );

        return empty($items) ? null : $items->eq(0);
    }
}
