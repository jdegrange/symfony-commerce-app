<?php

namespace App\Tests;

use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class HomepageControllerTest extends WebTestCase
{
    /**
     * Test to valid that homepage contains exactly 12 products
     */
    public function testHomepageContainsTwelveProducts(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertItemsCountEquals($crawler, 12);
    }

    /**
     * Test products count
     */
    private function assertItemsCountEquals(Crawler $crawler, $expectedCount): void
    {
        $actualCount = $crawler
            ->filter('.row .card-body')
            ->count();

        Assert::assertEquals(
            $expectedCount,
            $actualCount,
            "The homepage should contain \"$expectedCount\" product(s). Actual: \"$actualCount\" item(s)."
        );
    }
}
