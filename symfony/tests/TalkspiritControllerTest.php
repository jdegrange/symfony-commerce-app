<?php

namespace App\Tests;

use App\Controller\TalkspiritController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TalkspiritControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $client->request('GET', '/talkspirit');

        $response = $client->getResponse();
        $this->assertResponseIsSuccessful();

        $ret = $response->getContent();
        $ret = json_decode($ret, true);

        $this->assertArrayHasKey('title', $ret);

    }


    public function testFizzBuzz(): void{

        $controller = new TalkspiritController();

        $this->assertEquals('fizz', $controller->fizzBuzz(3));
        $this->assertEquals('buzz', $controller->fizzBuzz(5));
        $this->assertEquals('fizzBuzz', $controller->fizzBuzz(15));
        $this->assertEquals('7', $controller->fizzBuzz(7));

    }
}
